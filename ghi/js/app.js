function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <p class="card-text"><small class="text-muted">${starts} - ${ends}</small></p>

          </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      
    } else {
      const data = await response.json();
      
      let count = 0

      for (let conference of data.conferences) {
        

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const html = createCard(name, description, pictureUrl, starts, ends, location);
        if (count % 3 === 0) {
            const column = document.querySelector('.col1')
            column.innerHTML += html;
            }
            else if (count % 3 === 1) {
            const column = document.querySelector('.col2');
            column.innerHTML += html;
            } else {
            const column = document.querySelector('.col3');
            column.innerHTML += html;
            }
          count += 1
        }
      }
      console.log(count)
    }
  } catch (e) {
    
  }

});

