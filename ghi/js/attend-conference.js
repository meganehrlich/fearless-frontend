window.addEventListener('DOMContentLoaded', async () => {
  console.log("hello sweetie")
  
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
  
      for (let conference of data.conferences) {
        let option = document.createElement('option');

        option.value = conference.href;
        option.innerHTML = conference.name;

        selectTag.appendChild(option);
        
      }

      selectTag.classList.remove('d-none')
      const loadTag = document.getElementById("loading-conference-spinner")
      loadTag.classList.add('d-none')
    }

    console.log("Hello Sweetie")
    const attendeeFormTag = document.getElementById("create-attendee-form");
    attendeeFormTag.addEventListener('submit', async (event) => {
        event.preventDefualt();
        const attendeeData = new FormData(attendeeFormTag);
        const json = JSON.stringify(Object.fromEntries(attendeeData));
        console.log("Attendee": attendeeData)
        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {

            console.log("added")
            console.log(json)

            
        } 
    })
  });