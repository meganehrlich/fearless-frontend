* Create first fetch
* [x] For testing purposes add locations and conferences
* [x] Add code to create fetch in ghi/js/apps.js
* [x] Update code to get conference
* [x] Update code to get conference data
* [x] Update code to get conference description
  * Get the description data out of the object.
    Use the querySelector method to select the HTML element that should hold the description.
    Set the innerHTML property of the HTML element to the description of the conference.
    Remove all console.log statements when you're done.
* [x] Get image up
  * [x] add picture_url to properties in locationlistencoder
  * [x] reference picture_url in app.js

* [x] Create 3 columns to display conferences
  * [x] add name, description, start/end, and location
  * [x] style a bit
* [] Create rows

*[?] Add Alert

* [x] add new-location.html with html
* [x] add new-location.js
* [x] Add api_get_states to events/api_views
  * [x] register url
  * [x] test
* [x] new-location.js add states to dropdown menu
* [x] add functionality to form for new location
* [x] add link to nav bar to get to create new location page
* [x] add link to nav bar location page on location page
* [x] adjust navbars to show appropriate active page
* [ ] Here are steps that can help guide you in this part of the project:

* [] Create a new HTML page that will contain the form to create a new  conference.
* [] Choose the appropriate input type  for the different name, start date, end date, maximum presentations, and maximum attendees inputs.
* [] Use a textarea for the "Description" field. It should not be a "floating form" field. Refer to the Bootstrap form controls  documentation.
* [] Make sure to load a JavaScript file for creating new conferences.
* [] Create a new JavaScript file to handle loading the available * [] locations and submitting the conference data.
* [] The value of the options for the locations select tag should be the value of the "id" property of the location object. (This is comparable to what you did on the last page with setting the value of the option to the "abbreviation" property.)
* [] Notice that the JSON expects a numeric identifier for the location. * [] You'll need to change the location list API to include the id property of locations.
* [] Make sure you POST to the correct API URL.
* [] Clear the data from the form when it succeeds.
* [] Confirm that it works by creating a new conference, then checking the "List conferences" request in Insomnia.

